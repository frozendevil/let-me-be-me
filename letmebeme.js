// Adapted from Jeff Johnson's `autocomplete` Safari extension (here: http://lapcatsoftware.com/blog/2010/06/10/safari-extension-autocomplete/) which came with the following disclaimer:
// Adapted from http://userscripts.org/scripts/review/68645 by Jeff Johnson http://lapcatsoftware.com/
// Originally written by Andreas Huber http://andunix.net/ under http://creativecommons.org/licenses/by/3.0/de/deed.en
// Jeff simply deleted some logging lines from the script.
// Jeff is not releasing this under any license, because he hates that legal crap.

var badTags = ["autocomplete", "onpaste", "oncontextmenu", "oncut", "oncopy"];

function removeBadTags(element) {
	for (var i = 0; i < badTags.length; i++) {
		var badNode = element.getAttributeNode(badTags[i]);
		if (badNode != null) {
			element.removeAttributeNode(badNode);
		}
	}
}

function removeAllAutocomplete(elements) {
	for (var i = 0; i < elements.length; i++) {
		removeBadTags(elements[i]);
	}
}

removeAllAutocomplete(document.getElementsByTagName('form'));
removeAllAutocomplete(document.getElementsByTagName('input'));
