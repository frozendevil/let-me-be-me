Let Me Be Me
============

Removes autocomplete, onpaste, oncut, etc. attributes from webpages

Based on Jeff Johnson's `autocomplete` Safari extension (here: http://lapcatsoftware.com/blog/2010/06/10/safari-extension-autocomplete/)